import { v4 as uuid } from 'uuid';

export class DestinoViaje {
	private selected: boolean;
	public servicios: string[];
	public id = uuid();
	public votes = 0;

	constructor(public nombre: string, public imageUrl: string) {
		this.servicios = ['pileta', 'desayuno'];
	}

	isSelected(): boolean {
		return this.selected;
	}

	setSelected(s: boolean): void {
		this.selected = s;
	}

	voteUp(): void {
		this.votes++;
	}

	voteDown(): void {
		this.votes--;
	}

	voteReset(): void {
		this.votes = 0;
	}
}
